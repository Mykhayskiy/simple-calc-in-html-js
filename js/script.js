let display = document.getElementById("calc-display");
let calcNumbers = document.querySelectorAll(".calc__keys__key.number-key");
let separator = document.querySelector(".calc__keys__key.separator-key");
let operators = document.querySelectorAll(".calc__keys__key.operator-key");
let equal = document.querySelector(".calc__keys__key.equals-key");
let clearKey = document.querySelector(".calc__keys__key.clear-key");


function initSessionStorage() {
    sessionStorage.setItem('firstNumber', null);
    sessionStorage.setItem('secondNumber', null);
    sessionStorage.setItem('operator', null);
    sessionStorage.setItem('firstSeparator', null);
    sessionStorage.setItem('secondSeparator', null);
}

initSessionStorage();

clearKey.addEventListener('click', () => {
    initSessionStorage();
    display.innerText = "0";
});

calcNumbers.forEach(numberKey => {
    numberKey.addEventListener('click', () => {
        setFirstNumber(numberKey.textContent);
    });
});

operators.forEach(operatorKey => {
    operatorKey.addEventListener('click', () => {
        setOperator(operatorKey.textContent);
    });
});

separator.addEventListener('click', () => {
    let sessionOperator = sessionStorage.getItem('operator');
    let firstSeparator = sessionStorage.getItem('firstSeparator');
    let secondSeparator = sessionStorage.getItem('secondSeparator');
    let convertSep = ".";

    if (sessionOperator === null || sessionOperator === "null") {
        if (firstSeparator !== null && firstSeparator !== "null") {
            riseAlert('You can\'t have two separators in the same number');
            return;
        }
        sessionStorage.setItem('firstSeparator', convertSep);
        setFirstNumber(convertSep);
    } else {
        if (secondSeparator !== null && secondSeparator !== "null") {
            riseAlert('You can\'t have two separators in the same number');
            return;
        }
        sessionStorage.setItem('secondSeparator', convertSep);
        setSecondNumber(convertSep);
    }
});

function riseAlert(message) {
    alert(message);
}

function setFirstNumber(number) {
    let sessionFirstSeparator = getSessionFirstSeparator();
    let firstNumberTmp = sessionStorage.getItem('firstNumber');

    if (firstNumberTmp === null || firstNumberTmp === "null") {
        firstNumberTmp = '';
    }

    if (firstNumberTmp === '0' && sessionFirstSeparator === "null") {
        riseAlert('You can\'t start with double 0 without a separator');
        return;
    }

    if (sessionStorage.getItem('operator') !== null && sessionStorage.getItem('operator') !== "null") {
        setSecondNumber(number);
        return;
    }

    if (firstNumberTmp === '') {
        sessionStorage.setItem('firstNumber', number);
    }
    else {
        sessionStorage.setItem('firstNumber', firstNumberTmp + number);
    }
    updateDisplay();
}

function setSecondNumber(number) {
    let sessionSecondSeparator = getSessionSecondSeparator();
    let secondNumberTmp = sessionStorage.getItem('secondNumber');

    if (secondNumberTmp === null || secondNumberTmp === "null") {
        secondNumberTmp = '';
    }

    if (secondNumberTmp == '0' && sessionSecondSeparator == "null") {
        riseAlert('You can\'t start with double 0 without a separator');
        return;
    }
    sessionStorage.setItem('secondNumber', secondNumberTmp + number);
    updateDisplay();
}

function updateDisplay() {
    let firstNumberTmp = sessionStorage.getItem('firstNumber');
    let secondNumberTmp = sessionStorage.getItem('secondNumber');
    let operatorTmp = sessionStorage.getItem('operator');

    if (firstNumberTmp === null || firstNumberTmp === "null") {
        firstNumberTmp = '';
    }
    if (secondNumberTmp === null || secondNumberTmp === "null") {
        secondNumberTmp = '';
    }
    if (operatorTmp === null || operatorTmp === "null") {
        operatorTmp = '';
    }
    display.textContent = firstNumberTmp + operatorTmp + secondNumberTmp;
}

function setOperator(operator) {
    if (sessionStorage.getItem('firstNumber') === null || sessionStorage.getItem('firstNumber') === "null") {
        riseAlert('You need to enter a number first');
        return;
    }
    sessionStorage.setItem('operator', operator);
    updateDisplay();
}

function getSessionFirstSeparator() {
    return sessionStorage.getItem('firstSeparator');
}

function getSessionSecondSeparator() {
    return sessionStorage.getItem('secondSeparator');
}

equal.addEventListener('click', () => {
    let firstNumberTmp = sessionStorage.getItem('firstNumber');
    let secondNumberTmp = sessionStorage.getItem('secondNumber');
    let operatorTmp = sessionStorage.getItem('operator');

    if (firstNumberTmp === null || firstNumberTmp === "null") {
        riseAlert('You can\'t start with double 0 without a separator');
        return;
    }
    if (secondNumberTmp === null || secondNumberTmp === "null") {
        riseAlert('You can\'t start with double 0 without a separator');
        return;
    }
    if (operatorTmp === null || operatorTmp === "null") {
        riseAlert('You can\'t start with double 0 without a separator');
        return;
    }

    let result = calculate(firstNumberTmp, secondNumberTmp, operatorTmp);
    display.textContent = result;
});

function calculate(firstNumber, secondNumber, operator) {
    let result = 0;
    switch (operator) {
        case '+':
            result = parseFloat(firstNumber) + parseFloat(secondNumber);
            break;
        case '-':
            result = parseFloat(firstNumber) - parseFloat(secondNumber);
            break;
        case 'x':
            result = parseFloat(firstNumber) * parseFloat(secondNumber);
            break;
        case '/':
            result = parseFloat(firstNumber) / parseFloat(secondNumber);
            break;
        case '%':
            result = (parseFloat(secondNumber)/100) * parseFloat(firstNumber);
            break;
        default:
            alert('An error occurred');
    }
    return result;
}

